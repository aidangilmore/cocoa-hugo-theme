# DON'T USE THIS
This fork of Cocoa contains modifications to reduce unneeded functionality. Please instead get a proper copy of the theme from [Github](https://github.com/nishanths/cocoa-hugo-theme)